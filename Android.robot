*** Settings ***
Library           AppiumLibrary
Library           String
Library           ExcelRobot
Library           Collections

*** Test Cases ***
RunAndroid
    Open App

Mobile Google Map
    Map Open App

Mobile Chrome Extosoft
    Extosoft Open App

*** Keywords ***
Open App
    Set Library Search Order    AppiumLibrary
    Open Application    http://127.0.0.1:4723/wd/hub    deviceName=emulator-5554    platformName=Android    platformVersion=11    appPackage=com.google.android.youtube    appActivity=com.google.android.apps.youtube.app.application.Shell_HomeActivity    noReset=true
    ${xpath_video}    Set Variable    //android.view.ViewGroup[@content-desc="No one was adopting this cat. Then one family took a chance on her. - 3 minutes, 4 seconds - Go to channel - GeoBeats Animals - 205K views - 9 days ago - play video"]/android.view.ViewGroup[1]/android.widget.ImageView
    Wait Until Page Contains Element    //android.widget.ImageView[@content-desc="Search"]
    Sleep    2
    Click Element    //android.widget.ImageView[@content-desc="Search"]
    Wait Until Page Contains Element    //android.widget.EditText[@resource-id="com.google.android.youtube:id/search_edit_text"]
    Input Text    //android.widget.EditText[@resource-id="com.google.android.youtube:id/search_edit_text"]    Cat
    Press Keycode    66
    Sleep    3
    Run Keyword    Swipe    ${xpath_video}
    Click Element    ${xpath_video}
    Sleep    11
    Press Keycode    85
    Sleep    1
    Close Application

Swipe
    [Arguments]    ${xpath_video}
    FOR    ${i}    IN RANGE    0    1000000
        ${check}    Run Keyword And Return Status    Element Should Be Visible    ${xpath_video}
        Run Keyword If    '${check}' != 'True'    Swipe By Percent    50    80    50    20
        ...    ELSE    Exit For Loop
    END

Map Open App
    Set Library Search Order    AppiumLibrary
    Open Application    http://127.0.0.1:4723/wd/hub    deviceName=emulator-5554    platformName=Android    platformVersion=11    noReset=true    appPackage=com.google.android.apps.maps    appActivity=com.google.android.maps.MapsActivity
    Wait Until Page Contains Element    //android.widget.ImageButton[@content-desc="Directions"]
    Click Element    //android.widget.ImageButton[@content-desc="Directions"]
    Run Keyword    Map Excel Read
    Close Application

Map Excel Read
    Set Library Search Order    ExcelRobot
    Open Excel    D:\\Robot_Training\\Excel\\Travel.xlsx
    ${rowCountInput}    Get Row Count    Travel
    @{resultFormList}    Create List
    @{resulttoList}    Create List
    @{resultTimeList}    Create List
    @{resultDistanceList}    Create List
    Set Suite Variable    ${resultTimeList}
    Set Suite Variable    ${resultDistanceList}
    FOR    ${input}    IN RANGE    1    ${rowCountInput}
        ${tag}    Read Cell Data    Travel    0    ${input}
        Run Keyword If    '${tag}' != 'RUN'    Continue For Loop
        ${form}    Read Cell Data    Travel    1    ${input}
        ${to}    Read Cell Data    Travel    2    ${input}
        Append To List    ${resultFormList}    ${form}
        Append To List    ${resulttoList}    ${to}
        Comment    Run Auto Web
        BuiltIn.Run Keyword    Map Input Data    ${form}    ${to}
    END
    Run Keyword    Map Write to Excel    ${resultFormList}    ${resulttoList}

Map Input Data
    [Arguments]    ${form}    ${to}
    Wait Until Page Contains Element    //android.widget.EditText[@resource-id="com.google.android.apps.maps:id/directions_startpoint_textbox"]//android.widget.LinearLayout
    Sleep    2
    Click Element    //android.widget.EditText[@resource-id="com.google.android.apps.maps:id/directions_startpoint_textbox"]//android.widget.LinearLayout
    Wait Until Page Contains Element    com.google.android.apps.maps:id/search_omnibox_edit_text
    Input Text    com.google.android.apps.maps:id/search_omnibox_edit_text    ${form}
    Press Keycode    66
    Click Element    //android.widget.EditText[@resource-id="com.google.android.apps.maps:id/directions_endpoint_textbox"]//android.widget.LinearLayout
    Wait Until Page Contains Element    com.google.android.apps.maps:id/search_omnibox_edit_text
    Input Text    com.google.android.apps.maps:id/search_omnibox_edit_text    ${to}
    Press Keycode    66
    Run Keyword    Map Get Data

Map Get Data
    Wait Until Page Contains Element    //android.widget.LinearLayout//android.widget.LinearLayout//android.widget.TextView[2]
    ${time}    Get Text    //android.widget.LinearLayout[@resource-id="com.google.android.apps.maps:id/sheet_header"]//android.widget.LinearLayout//android.widget.LinearLayout[1]//android.widget.TextView[1]
    Append To List    ${resultTimeList}    ${time}
    ${distance}    Get Text    //android.widget.LinearLayout//android.widget.LinearLayout//android.widget.TextView[2]
    ${distance}    Remove String    ${distance}    (    )
    Append To List    ${resultDistanceList}    ${distance}
    Set Suite Variable    ${resultTimeList}
    Set Suite Variable    ${resultDistanceList}

Map Write to Excel
    [Arguments]    ${resultFormList}    ${resulttoList}
    Set Library Search Order    ExcelRobot
    ${rowCountOutput}    Get Length    ${resultFormList}
    Open Excel To Write    D:\\Robot_Training\\Excel\\Travel.xlsx    new_path=D:\\Robot_Training\\Excel\\Travel_Copy.xlsx    override=override
    FOR    ${output}    IN RANGE    0    ${rowCountOutput}
        ${i}    Evaluate    ${output} + 2
        Write To Cell By Name    OUTPUT    A${i}    ${resultFormList}[${output}]    data_type=TEXT
        Write To Cell By Name    OUTPUT    B${i}    ${resulttoList}[${output}]    data_type=TEXT
        Write To Cell By Name    OUTPUT    C${i}    ${resultTimeList}[${output}]    data_type=TEXT
        Write To Cell By Name    OUTPUT    D${i}    ${resultDistanceList}[${output}]    data_type=TEXT
    END
    Save Excel

Extosoft Open App
    Set Library Search Order    AppiumLibrary
    Open Application    http://127.0.0.1:4723/wd/hub    deviceName=emulator-5554    platformName=Android    platformVersion=11    noReset=true    appPackage=com.android.chrome    appActivity=org.chromium.chrome.browser.ChromeTabbedActivity
    Run Keyword    Extosoft Excel Read
    Close Application

Extosoft Excel Read
    Set Library Search Order    ExcelRobot
    Open Excel    D:\\Robot_Training\\Excel\\ExtoSoft.xlsx
    ${rowCountInput}    Get Row Count    INPUT
    @{resultCompanyList}    Create List
    @{CompanyList}    Create List
    @{AddressList}    Create List
    @{PhonenumberList}    Create List
    @{EmailList}    Create List
    Set Suite Variable    ${CompanyList}
    Set Suite Variable    ${AddressList}
    Set Suite Variable    ${PhonenumberList}
    Set Suite Variable    ${EmailList}
    FOR    ${input}    IN RANGE    1    ${rowCountInput}
        ${tag}    Read Cell Data    INPUT    0    ${input}
        Run Keyword If    '${tag}' != 'RUN'    Continue For Loop
        ${company}    Read Cell Data    INPUT    1    ${input}
        Append To List    ${resultCompanyList}    ${company}
        BuiltIn.Run Keyword    Extosoft Input Data    ${company}
    END
    Run Keyword    Extosoft Write to Excel

Extosoft Input Data
    [Arguments]    ${company}
    Wait Until Page Contains Element    com.android.chrome:id/home_button
    Click Element    com.android.chrome:id/home_button
    Wait Until Page Contains Element    com.android.chrome:id/search_box_text
    Sleep    2
    Click Element    com.android.chrome:id/search_box_text
    Wait Until Page Contains Element    com.android.chrome:id/url_bar
    Input Text    com.android.chrome:id/url_bar    ${company}
    Press Keycode    66
    Run Keyword    Extosoft Get Data

Extosoft Get Data
    Sleep    3
    FOR    ${i}    IN RANGE    0    1000000
        ${check}    Run Keyword And Return Status    Element Should Be Visible    //*[contains(@content-desc,"Extosoft https://extosoft.com")]/*[2]
        Run Keyword If    '${check}' != 'True'    Swipe By Percent    50    80    50    70
        ...    ELSE    Exit For Loop
    END
    Click Element    //*[contains(@content-desc,"Extosoft https://extosoft.com")]/*[2]
    Wait Until Page Contains Element    //*[@content-desc="Extosoft"]
    FOR    ${i}    IN RANGE    0    1000000
        ${check}    Run Keyword And Return Status    Element Should Be Visible    //*[@content-desc="support@extosoft.com"]/*[2]
        Run Keyword If    '${check}' != 'True'    Swipe By Percent    50    80    50    50
        ...    ELSE    Exit For Loop
    END
    Wait Until Page Contains Element    //*[@content-desc="support@extosoft.com"]/*[2]
    ${text}    Get Text    //*[@resource-id="bt_bb_section645227c2e695e"]/*/*[1]/*[2]
    ${company}    Remove String    ${text}    52/12 soi Ladprao 101 soi 46 (kittichit), Ladprao rd., Khlong Chan, Bang Kapi, Bangkok 10240    \n
    ${address}    Remove String    ${text}    Extosoft Co.,Ltd.    \n
    ${phonenumber}    Get Text    //*[@content-desc="025506271"]/*[2]
    ${email}    Get Text    //*[@content-desc="support@extosoft.com"]/*[2]
    Append To List    ${CompanyList}    ${company}
    Append To List    ${AddressList}    ${address}
    Append To List    ${PhonenumberList}    ${phonenumber}
    Append To List    ${EmailList}    ${email}
    Set Suite Variable    ${CompanyList}
    Set Suite Variable    ${AddressList}
    Set Suite Variable    ${PhonenumberList}
    Set Suite Variable    ${EmailList}

Extosoft Write to Excel
    Set Library Search Order    ExcelRobot
    ${rowCountOutput}    Get Length    ${CompanyList}
    Open Excel To Write    D:\\Robot_Training\\Excel\\Extosoft.xlsx    new_path=D:\\Robot_Training\\Excel\\Extosoft_Copy.xlsx    override=override
    FOR    ${output}    IN RANGE    0    ${rowCountOutput}
        ${i}    Evaluate    ${output} + 2
        Write To Cell By Name    OUTPUT    A${i}    ${CompanyList}[${output}]    data_type=TEXT
        Write To Cell By Name    OUTPUT    B${i}    ${AddressList}[${output}]    data_type=TEXT
        Write To Cell By Name    OUTPUT    C${i}    ${PhonenumberList}[${output}]    data_type=TEXT
        Write To Cell By Name    OUTPUT    D${i}    ${EmailList}[${output}]    data_type=TEXT
    END
    Save Excel
